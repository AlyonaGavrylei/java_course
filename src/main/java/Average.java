public class Average {
    public static void main (String [] args){
        int[] numbers = {5, 8, 12, 7, 15};

        double sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        double average = sum / numbers.length;
        System.out.println("The average value of this massive is " + average);
    }
}
